//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
  var movimientosJSONv2 = require ('./movimientosv2.json')

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  //res.send ("HOLA MUNDO DESDE NODE.js");
  res.sendFile(path.join(__dirname,'index.html'));
});

//Se puede sobreescribir la url
app.post('/', function (req,res) {
  res.send ("HEMOS RECIBIDO LA PETICION POST");
});


//Se puede sobreescribir la url
app.put('/', function (req,res) {
  res.send ("HEMOS RECIBIDO LA PETICION PUT / UPDATE ");
});


//Se puede sobreescribir la url
app.delete('/', function (req,res) {
  res.send ("HEMOS RECIBIDO LA PETICION DELETE / BORRADO ");
});


//Se puede sobreescribir la url, proporcionando el idcliente
app.get('/Clientes/:idcliente', function (req, res){
  res.send (" AQUI SE MUESTRA AL CLIENTE:"+ req.params.idcliente);
});


// es senfile con minuscula porque no definimos el path
app.get('/v1/Movimientos', function(req, res) {
  res.sendfile ('movimientosv1.json');

});



// es senfile con minuscula porque no definimos el path
app.get('/v2/Movimientos', function(req, res) {
  res.json (movimientosJSONv2);

});


// es senfile con minuscula porque no definimos el path
app.get('/v2/Movimientos/:id', function(req, res) {
  console.log ( req.params.id);
  res.send(movimientosJSONv2[req.params.id-1]);

});


app.get('/v2/Movimientos/:id/:nombre', function(req, res) {
  console.log ( req.params);
  console.log ( req.params.id);
  console.log ( req.params.nombre);
  res.send("RECIBIDO CON MAS PARAMETROS");

});


// Movimientos Query
app.get('/v2/Movimientosq', function(req, res) {
  console.log ( req.query);
  res.send ("Peticion Recibida [PRUEBA CON DOCKER]"+req.query);
});
